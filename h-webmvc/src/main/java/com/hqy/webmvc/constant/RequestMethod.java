package com.hqy.webmvc.constant;

//请求方法枚举类
public enum RequestMethod {
    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
}