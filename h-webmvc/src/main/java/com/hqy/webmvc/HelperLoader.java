package com.hqy.webmvc;

import com.hqy.aop.helper.AopHelper;
import com.hqy.bean.helper.BeanHelper;
import com.hqy.core.helper.ClassHelper;
import com.hqy.ioc.helper.IocHelper;
import com.hqy.tools.utils.ClassUtil;
import com.hqy.webmvc.helper.ControllerHelper;

public final class HelperLoader {

    public static void init() {
        Class<?>[] classList = {
            ClassHelper.class,
            BeanHelper.class, AopHelper.class,
            IocHelper.class,
            ControllerHelper.class,


        };
        for (Class<?> cls : classList) {
            ClassUtil.loadClass(cls.getName());
        }
    }
}
