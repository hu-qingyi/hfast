package com.hqy.core.constant;

public interface ConfigConstant {

    //配置文件的名称
    String CONFIG_FILE = "settings.properties";

    //数据源
    String JDBC_DRIVER = "jdbc.driver";
    String JDBC_URL = "jdbc.url";
    String JDBC_USERNAME = "jdbc.username";
    String JDBC_PASSWORD = "jdbc.password";

    //java源码地址
    String APP_BASE_PACKAGE = "hfast.base_package";
    //jsp页面路径
    String APP_JSP_PATH = "hfast.jsp_path";
    //静态资源路径
    String APP_ASSET_PATH = "hfast.staic_path";
}
