package com.hqy.bean.annotation.itf;

/**
 * @Author hqy
 * @Date 2022/5/15
 * @Description
 */
@FunctionalInterface
public interface Condition {

    boolean matches();
}
