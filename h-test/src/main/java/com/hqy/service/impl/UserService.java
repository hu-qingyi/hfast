package com.hqy.service.impl;


import com.hqy.bean.annotation.Conditional;
import com.hqy.bean.annotation.Service;
import com.hqy.conditionalTest.LoadConditional;
import com.hqy.entity.User;
import com.hqy.jdbc.helper.DatabaseHelper;
import com.hqy.service.IUserService;
import com.hqy.tx.annotation.Transactional;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Conditional(LoadConditional.class)
public class UserService implements IUserService {
    /**
     * 获取所有用户
     */
    public List<User> getAllUser() {
        String sql = "SELECT * FROM user";
        return DatabaseHelper.queryEntityList(User.class, sql);
    }

    /**
     * 根据id获取用户信息
     */
    public User GetUserInfoById(Integer id) {
        String sql = "SELECT * FROM user WHERE id = ?";
        return DatabaseHelper.queryEntity(User.class, sql, id);
    }

    /**
     * 修改用户信息
     */
    public boolean updateUser(int id, Map<String, Object> fieldMap) {
        return DatabaseHelper.updateEntity(User.class, id, fieldMap);
    }

}
