package com.hqy.service;

import com.hqy.entity.User;

import java.util.List;
import java.util.Map;

public interface IUserService {

    List<User> getAllUser();

    User GetUserInfoById(Integer id);

    boolean updateUser(int id, Map<String, Object> fieldMap);
}

