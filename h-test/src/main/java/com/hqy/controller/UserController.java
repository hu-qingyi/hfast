package com.hqy.controller;

import com.hqy.bean.annotation.Controller;
import com.hqy.entity.User;
import com.hqy.ioc.annotation.Autowired;
import com.hqy.service.IUserService;
import com.hqy.tx.annotation.Transactional;
import com.hqy.webmvc.annotation.RequestMapping;
import com.hqy.webmvc.constant.RequestMethod;
import com.hqy.webmvc.entity.Data;
import com.hqy.webmvc.entity.Param;
import com.hqy.webmvc.entity.View;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    private IUserService userService;

    /**
     * 用户列表
     *
     * @return
     */
    @RequestMapping(value = "/userList", method = RequestMethod.GET)
    public View getUserList() {
        List<User> userList = userService.getAllUser();
        return new View("index.jsp").addModel("userList", userList);
    }

    /**
     * 用户详情
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public Data getUserInfo(Param param) {
        String id = (String) param.getParamMap().get("id");
        User user = userService.GetUserInfoById(Integer.parseInt(id));

        return new Data(user);
    }

    @RequestMapping(value = "/t", method = RequestMethod.GET)
    @Transactional
    public Data editUser(Param param) {
        String id = (String) param.getParamMap().get("id");
        Map<String, Object> fieldMap = new HashMap<>();
        fieldMap.put("age", 911);
        int i=1/0;
        userService.updateUser(Integer.parseInt(id), fieldMap);
        fieldMap.put("age", 100);
        userService.updateUser(Integer.parseInt(id), fieldMap);
        return new Data("Success.");
    }
}
